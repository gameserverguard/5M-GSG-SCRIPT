# 5M-GSG-ASSETS TO INSTALLER - GAME SERVER GUARD

SCRIPT de autoinstalación NGINX para FiveM / RedM

## Requisitos

- Debian 10/11.
- Acceso Root.
- Servidor FiveM/RedM.
- **Minima experiencia usando NGINX.**
- **UN DOMINIO CONFIGURADO CON CLOUDFLARE.**
- **CERTIFICADOS ORIGIN PUBLIC / ORIGIN PRIVATE en archivo .PEM (public.pem / private.pem)**

## Instalacion

1. Descargar
2. Convertir el sh en ejecutable: `chmod +x install.sh`
3. Ejecutar: `./install.sh`

## Notas adicionales

- **Si usas UFW debes añadir los puertos que vayas a usar para el tunnel.**
- Si desea utilizar un puerto personalizado, simplemente consulte `reverseproxy/reverse.conf` para editar lo que necesita editarse.
- Si desea cambiar parametro del dominio, simplemente consulte `dominios/dominio.conf` para editar lo que necesita editarse.
- Este proxy **NO ES UN ANTI-DDOS**, solo es un proxy reverso con cache. 
- La protección dependerá del proveedor del **VPS/DEDICADO** que uses para el proxy.

## ¿COMO TESTEAR FUNCIONAMIENTO?

Tu dominio debe responder a estos de la siguiente manera :

- `https://tudominio.com/info.json` (debe responder informacion)
- `https://tudominio.com/players.json` (debe responder listado de jugadores)
- `https://tudominio.com/dynamic.json` (debe responder informacion)
- `https://tudominio.com/client` (debe responder '/client is POST only')

## SERVERCFG - Cache Config

- `fileserver_remove ".*"`
- `fileserver_add ".*" "https://tudominio.com/files"` <- Dirección a tu dominio a la carpeta /files (definido en el NGINX)
- `set sv_downloadUrl "https://tudominio.com/files"`

`adhesive_cdnKey "C14V34L34T0R1A"` <- Una clave aleatoria personal del servidor, puedes generarla con `https://randomkeygen.com` para evitar corrupciones.

## SERVERCFG - Server Config

- La siguiente configuración es solo si el servidor cache va de forma externa al tunnel (dos servidores)

Colocar esta configuración en el servidor principal.

- set `sv_listingIpOverride "IPdelProxyServer ó IPdelServer"`
- set `sv_proxyIPPranges "IPdelProxyServer/32 ó IPdelServer/32"`
- set `sv_listingHostOverride tudominio.com #dominio del servidor.`
- set `sv_requestParanoia 0`
- set `sv_endpoints "IPdelProxyServer:30120 ó IPdelServer:30120"` #Aqui debes poner la IP de tu proxy tunnel o tu servidor principal.

## X/O - GAME SERVER GUARD